class Tarjeta
  
  def self.cargar_tarjeta(id, saldo_a_cargar)
    tarjetas[id] ||= 0.0
    tarjetas[id] = tarjetas[id] + saldo_a_cargar
  end

  def initialize(id)
    @id = id
    @saldo = Tarjeta.tarjetas[id] || 0.0
  end

  def saldo_suficiente?(monto)
    monto <= @saldo
  end

  def debitar(monto)
    @saldo -= monto
    Tarjeta.tarjetas[@id] = @saldo
  end

  private
    @@tarjetas = {}

    def self.tarjetas
      @@tarjetas
    end

    def saldo
      @saldo
    end
end
