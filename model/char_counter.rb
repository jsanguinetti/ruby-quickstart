require 'active_support'

class CharCounter

  def count(str)
    return {} if str.nil? || str.empty?
    I18n.transliterate(str)
      .gsub(' ', '')
      .downcase
      .split('')
      .map(&:to_sym)
      .reduce({}) do |res, char|
        res[char] ||= 0
        res[char] = res[char] + 1
        res
      end
  end

end
