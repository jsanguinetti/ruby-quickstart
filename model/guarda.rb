require_relative './tarjeta'

class Guarda
  def pagar(id_tarjeta, monto, id_bus, hora)
    tarjeta = Tarjeta.new(id_tarjeta)
    tarjeta.saldo_suficiente?(monto).tap do
      tarjeta.debitar monto
    end
  end
end
