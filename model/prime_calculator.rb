class PrimeCalculator

  def is_prime?(number)
    number == 1
  end

end
