require 'rspec' 
require_relative '../model/char_counter'

describe 'CharCounter' do

  describe 'count' do
    let(:char_counter) { CharCounter.new }

    it { expect(char_counter.count 'hoge hoge').to eq({ h: 2, o: 2, g: 2, e: 2 }) }
    it { expect(char_counter.count 'nicolas').to eq({ n: 1, o: 1, i: 1, c: 1, l: 1, a: 1, s: 1 }) }
    it { expect(char_counter.count nil).to eq({}) }
    it { expect(char_counter.count '').to eq({}) }
    it { expect(char_counter.count 'Mama').to eq({m: 2, a: 2}) }
    it { expect(char_counter.count 'Mamá').to eq({m: 2, a: 2}) }
    it { expect(char_counter.count 'ma ma').to eq({m: 2, a: 2}) }
  end

end
