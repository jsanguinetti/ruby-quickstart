require 'rspec' 
require_relative '../model/guarda'

describe 'Guarda' do

  describe 'pagar' do
    let!(:now) { Time.now }
    let(:guarda) { Guarda.new }
    subject { guarda.pagar *args }
    let(:args) { [123456789, 29.0, '185', now] }

    context 'saldo suficiente' do
      before { cargar_tarjeta(123456789, 29.0) }

      it { should be_truthy }
    end
    
    context 'saldo insuficiente' do
      before { cargar_tarjeta(123456789, 20.0) }
      
      it { should be_falsy }
    end

    describe 'la tarjeta se debita' do
      before do 
        cargar_tarjeta(123456789, 29.0)
        guarda.pagar *args
      end
    
      it 'should not permitir pagar sin saldo' do
        expect(subject).to be_falsy
      end
    end
  end

end

def cargar_tarjeta(tarjeta_id, saldo_a_cargar)
  Tarjeta.cargar_tarjeta(tarjeta_id, saldo_a_cargar)
end
