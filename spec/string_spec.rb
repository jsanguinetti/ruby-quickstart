require 'rspec' 

describe 'String' do
  let(:str) { 'ThIS is A StrING' }
  describe 'downcase' do
    subject { str.downcase }

    it { should eq 'this is a string' }
  end

  describe 'upcase' do
    subject { str.upcase }
    
    it { should eq 'THIS IS A STRING' }
  end

  describe 'gsub' do
    subject { str.gsub('h', 'SOY UNA HACHE') }

    it { should eq 'TSOY UNA HACHEIS is A StrING'}
  end

  describe 'reverse' do
    subject { str.reverse }

    it { should eq 'GNIrtS A si SIhT' }
  end

  describe 'split' do
    subject { str.split(' ') }

    it { should eq %w(ThIS is A StrING) }
  end
  
end
