require 'rspec' 
require_relative '../model/tarjeta'

describe 'Tarjeta' do
  let(:id) { 123456789 }

  describe 'self.cargar_tarjeta' do

    it 'should cargar tarjeta con 29.0 pesos' do
      expect {
        Tarjeta.cargar_tarjeta(id, 29.0)
      }.to change {
        Tarjeta.send(:tarjetas)[id]
      }.from(nil).to(29.0)
    end
  end

  describe 'saldo_suficiente?' do
    before { Tarjeta.cargar_tarjeta(id, 29.0) }

    it { expect(Tarjeta.new(id).saldo_suficiente? 29.0).to be_truthy }
  end

  describe 'debitar' do
    let(:tarjeta) { Tarjeta.new id }
    before { Tarjeta.cargar_tarjeta(id, 29.0) }
    
    it do
      expect { 
        tarjeta.debitar 29.0
      }.to change {
        tarjeta.send :saldo
      }.from(29.0).to(0.0)
    end
  end

end
