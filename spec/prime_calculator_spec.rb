require 'rspec' 
require_relative '../model/prime_calculator'

describe 'PrimeCalculator' do

  describe 'is_prime?' do
    let(:calculator) { PrimeCalculator.new }

    it { expect(calculator.is_prime? 1).to be_truthy }
    it { expect(calculator.is_prime? 4).to be_falsy }
  end

end
